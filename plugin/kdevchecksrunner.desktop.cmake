[Desktop Entry]
Type=Service
Name=KDevChecksRunner
GenericName=KDevelop Checks Runner
Comment=Helps you run static checks against your code.
ServiceTypes=KDevelop/Plugin
X-KDE-Library=kdevchecksrunnerplugin
X-KDE-PluginInfo-Name=kdevchecksrunner
X-KDE-PluginInfo-Author=Aleix Pol
X-KDE-PluginInfo-Email=aleixpol@kde.org
X-KDE-PluginInfo-License=GPL
X-KDevelop-Version=@KDEV_PLUGIN_VERSION@
X-KDevelop-Category=Global
X-KDevelop-Mode=GUI
