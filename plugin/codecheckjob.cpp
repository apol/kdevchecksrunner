	/* This file is part of KDevelop
    Copyright 2010 Aleix Pol Gonzalez <aleixpol@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "codecheckjob.h"
#include <KDebug>
#include <kross/core/action.h>
#include <kross/core/manager.h>
#include <interfaces/icore.h>
#include <QStandardItemModel>
#include <KLocalizedString>
#include <interfaces/iplugincontroller.h>
#include <interfaces/ilanguagecheckprovider.h>
#include <interfaces/iassistant.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>
#include <language/checks/controlflowgraph.h>
#include <language/checks/dataaccessrepository.h>
#include <language/duchain/problem.h>
#include <QThread>

using namespace KDevelop;

Q_DECLARE_METATYPE(IAssistantAction::Ptr);

CodeCheckJob::CodeCheckJob(const KDevelop::ReferencedTopDUContext& top, QStandardItemModel* logModel, QObject* parent)
    : KJob(parent), m_logModel(logModel)
{
    m_data.top = top;
    m_data.url = top->url().toUrl();
    setObjectName(i18n("Running tests for %1", m_data.url.prettyUrl()));
}

CodeCheckJob::~CodeCheckJob()
{
	delete m_data.flow;
	delete m_data.access;
}

void CodeCheckJob::setAdditionalData(ControlFlowGraph* controlFlowGraph, DataAccessRepository* dataAccessInformation)
{
	m_data.flow=controlFlowGraph;
	m_data.access=dataAccessInformation;
}

void CodeCheckJob::start()
{
	kDebug() << "running checks for" << m_data.url;
	QMetaObject::invokeMethod(this, "runChecks");
}

void CodeCheckJob::runChecks()
{
    
	//check interfaces lookup
	QList< IPlugin* > plugins = ICore::self()->pluginController()->allPluginsForExtension("org.kdevelop.ILanguageCheck");
	QList<ILanguageCheck*> checks;
	foreach(IPlugin* p, plugins) {
		checks += p->extension<ILanguageCheck>();
	}
	
	QList< IPlugin* > providerPlugins = ICore::self()->pluginController()->allPluginsForExtension("org.kdevelop.ILanguageCheckProvider");
	foreach(IPlugin* p, providerPlugins) {
		ILanguageCheckProvider* check = p->extension<ILanguageCheckProvider>();
		checks += check->providedChecks();
	}
	
	if(!m_onlyCheck.isEmpty()) {
		ILanguageCheck* theCheck = 0;
		foreach(ILanguageCheck* aCheck, checks) {
			if(aCheck->name()==m_onlyCheck) {
				theCheck=aCheck;
				break;
			}
		}
		
		checks.clear();
		if(theCheck) {
			checks << theCheck;
		}
	}
	
	if(checks.isEmpty()) {
		kDebug() << "No checks found";
		checksRunned();
	} else {
		QStandardItem* testItem = new QStandardItem(i18n("Running tests for %1", m_data.top->url().str()));
		CheckJobThread* athread = new CheckJobThread(&m_data, testItem, checks, this);
		connect(athread, SIGNAL(finished()), SLOT(checksRunned()));
		athread->start();
	}
}

void CodeCheckJob::checksRunned()
{
	CheckJobThread* thread = qobject_cast<CheckJobThread*>(sender());
	
	if(thread) {
		m_logModel->appendRow(thread->item());
		emit statistics(thread->statistics());
	} else
		m_logModel->appendRow(new QStandardItem(i18n("No checks found")));
    emitResult();
}

////////////

CheckJobThread::CheckJobThread(CheckData* data, QStandardItem* item, const QList< ILanguageCheck* >& checks, QObject* parent)
	: QThread(parent), m_data(data), testItem(item), m_checks(checks)
{}

void CheckJobThread::run()
{
	QTime t;
	t.start();
	
	QStandardItem* checksItem = new QStandardItem(i18n("Checks"));
	foreach(ILanguageCheck* check, m_checks) {
		DUChainReadLocker lock(DUChain::lock());
		int problems=m_data->top->problems().size();
		int startTime=t.elapsed();
		lock.unlock();

		check->runCheck(*m_data);

		lock.lock();
		int probCount=m_data->top->problems().size()-problems;
		checksItem->appendRow(new QStandardItem(i18n("%1. found %2 problems", check->name(), probCount)));
		
		m_stats.insert(check->name()+"/ProblemsFound", probCount);
		m_stats.insert(check->name()+"/Elapsed", t.elapsed() - startTime);
	}
	int timeTaken = t.elapsed();
	
	QStandardItem* problemsItem = new QStandardItem(i18n("Problems"));
	DUChainReadLocker lock;
	QList<ProblemPointer> problems = m_data->top->problems();
	
	foreach(const ProblemPointer& p, problems) {
		QStandardItem* it=new QStandardItem(i18n("%1 %2", p->sourceString(), p->description()));
        it->setToolTip(it->text());
		it->setData(qVariantFromValue(p->finalLocation().textRange()));
        KSharedPtr< IAssistant > solution = p->solutionAssistant();
        if(solution) {
            QList<IAssistantAction::Ptr> actions = solution->actions();
            if(actions.count()==1) {
                it->setIcon(KIcon("tools-report-bug"));
                it->setData(qVariantFromValue<IAssistantAction::Ptr>(actions.first()), Qt::UserRole+1);
            }
        }
        problemsItem->appendRow(it);
	}
	
	testItem->setText(i18n("%1 %2", testItem->text(), timeTaken));
	testItem->setData(QUrl(m_data->url));
	testItem->appendRow(checksItem);
	testItem->appendRow(problemsItem);
	m_stats.insert(m_data->url.prettyUrl()+"/Elapsed", timeTaken);
}
