/*  This file is part of KDevelop
    Copyright 2010 Aleix Pol <aleixpol@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef KDEVCHECKSRUNNERPLUGIN_H
#define KDEVCHECKSRUNNERPLUGIN_H

#include <interfaces/iplugin.h>
#include <QVariant>
#include <QStandardItemModel>
#include <KUrl>
#include <kio/udsentry.h>

namespace KIO {
class Job;
}

class QModelIndex;
class KUrl;
class KJob;

namespace KDevelop
{
	class IProject;
	class IDocument;
	class ParseJob;
}

class KDevChecksRunnerPlugin : public KDevelop::IPlugin
{
	Q_OBJECT
	Q_PROPERTY(bool pause READ isPaused WRITE setPause NOTIFY pauseChanged)
	Q_PROPERTY(QString onlyCheck READ onlyCheck WRITE setOnlyCheck)
	public:
		KDevChecksRunnerPlugin(QObject *parent, const QVariantList & args);
		
		QAbstractItemModel* model() const { return m_logModel; }
		
		bool isPaused() const { return m_pause; }
		QString onlyCheck() const { return m_onlyCheck; }
		void setOnlyCheck(const QString& name) { m_onlyCheck=name; }
		
		enum DisplayMode { Editing, GlobalReport };
		void setDisplayMode(DisplayMode m) { m_displayMode=m; }
		DisplayMode displayMode() const { return m_displayMode; }
	public slots:
		void setPause(bool p) { m_pause = p;  emit pauseChanged(p); }
		void exportReport();
		bool exportReport(const QString& file);
		void parsingFinished(KDevelop::ParseJob*);
		void reloadAll();
		void clearOutput();
		void reloadProject(KDevelop::IProject* p);
		void addStatistics(const QVariantMap& stats);
        void executeAction();
		
		void gotoItem(const QModelIndex& idx);
		void changeToEditingMode(KDevelop::IDocument *doc);
		
	signals:
		void pauseChanged(bool);
		
	private:
		QStandardItemModel* m_logModel;
		bool m_pause;
		QString m_onlyCheck;
		QVariantMap m_stats;
		DisplayMode m_displayMode;
};

#endif
