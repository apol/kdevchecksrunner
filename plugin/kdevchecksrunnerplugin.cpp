/*  This file is part of KDevelop
    Copyright 2010 Aleix Pol <aleixpol@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "kdevchecksrunnerplugin.h"
#include <kpluginfactory.h>
#include <KAboutData>
#include <interfaces/iuicontroller.h>
#include <QAction>
#include <QHeaderView>
#include <QTreeView>
#include <language/duchain/topducontext.h>
#include <language/duchain/duchainlock.h>
#include <interfaces/icore.h>
#include <interfaces/idocumentcontroller.h>
#include <language/backgroundparser/parsejob.h>
#include <KIcon>
#include <interfaces/ilanguagecontroller.h>
#include <interfaces/ilanguage.h>
#include <language/interfaces/ilanguagesupport.h>
#include <language/duchain/duchain.h>
#include <language/backgroundparser/backgroundparser.h>
#include <KJob>
#include <KIO/Job>
#include "codecheckjob.h"
#include <interfaces/iruncontroller.h>
#include <language/checks/controlflowgraph.h>
#include <language/checks/dataaccessrepository.h>
#include <language/duchain/problem.h>
#include <interfaces/iprojectcontroller.h>
#include <interfaces/iproject.h>
#include <interfaces/iassistant.h>
#include <KFileDialog>
#include <kdebug.h>

K_PLUGIN_FACTORY(KDevChecksRunnerFactory, registerPlugin<KDevChecksRunnerPlugin>(); )
K_EXPORT_PLUGIN(KDevChecksRunnerFactory(
    KAboutData("kdevchecksrunner","kdevchecksrunner",
               ki18n("KDevelop Checks Runner"), "0.2", ki18n("Runs static checks to code"), KAboutData::License_GPL)))

using namespace KDevelop;

Q_DECLARE_METATYPE(IAssistantAction::Ptr);

class KDevChecksRunnerViewFactory: public KDevelop::IToolViewFactory
{
	public:
		KDevChecksRunnerViewFactory( KDevChecksRunnerPlugin *plugin ): mplugin( plugin ) {}
		
		virtual QWidget* create( QWidget *parent = 0 )
		{
			QTreeView* view=new QTreeView(parent);
			view->header()->setVisible(false);
			view->setModel(mplugin->model());
			view->setEditTriggers(QAbstractItemView::NoEditTriggers);
			view->setSortingEnabled(true);
// 			view->setRootIsDecorated(false);
			
			QAction* reloadAction = new QAction(KIcon("view-refresh"), i18n("Reload"), view);
			reloadAction->setToolTip(i18n("Runs all project analysis"));
			QObject::connect(reloadAction, SIGNAL(triggered()), mplugin, SLOT(reloadAll()));
			view->addAction(reloadAction);
			
			QAction* clearAction = new QAction(KIcon("edit-clear"), i18n("Clear Output"), view);
			clearAction->setToolTip(i18n("Clears the view"));
			QObject::connect(clearAction, SIGNAL(triggered()), mplugin, SLOT(clearOutput()));
			view->addAction(clearAction);
			
			QAction* pauseAction = new QAction(KIcon("media-playback-pause"), i18n("Un-pause"), view);
			pauseAction->setCheckable(true);
			pauseAction->setChecked(mplugin->isPaused());
			QObject::connect(pauseAction, SIGNAL(toggled(bool)), mplugin, SLOT(setPause(bool)));
			QObject::connect(mplugin, SIGNAL(pauseChanged(bool)), pauseAction, SLOT(setChecked(bool)));
			view->addAction(pauseAction);
			
			QAction* exportAction = new QAction(KIcon("document-export"), i18n("Export Report"), view);
			QObject::connect(exportAction, SIGNAL(triggered(bool)), mplugin, SLOT(exportReport()));
			view->addAction(exportAction);
			
            QAction* executeAction = new QAction(KIcon("games-solve"), i18n("Solve"), view);
            QObject::connect(executeAction, SIGNAL(triggered(bool)), mplugin, SLOT(executeAction()));
            view->addAction(executeAction);
            
			QObject::connect(mplugin->model(), SIGNAL(rowsInserted(QModelIndex,int,int)), view, SLOT(expandAll()));
			QObject::connect(view, SIGNAL(doubleClicked(QModelIndex)), mplugin, SLOT(gotoItem(QModelIndex)));
			return view;
		}
		
		virtual Qt::DockWidgetArea defaultPosition() { return Qt::LeftDockWidgetArea; }
		
		virtual QString id() const { return "org.kdevelop.KDevChecksRunnerPlugin"; }
		
	private:
		KDevChecksRunnerPlugin *mplugin;
};

KDevChecksRunnerPlugin::KDevChecksRunnerPlugin(QObject* parent, const QVariantList&)
    : KDevelop::IPlugin(KDevChecksRunnerFactory::componentData(), parent)
	, m_pause(false)
{
	KDebug::registerArea("kdevchecksrunnerplugin");
	m_logModel=new QStandardItemModel(this);
	
	core()->uiController()->addToolView(i18n("Language Checks"), new KDevChecksRunnerViewFactory(this));
	connect(core()->languageController()->backgroundParser(), SIGNAL(parseJobFinished(KDevelop::ParseJob* )),
            this, SLOT(parsingFinished(KDevelop::ParseJob* )));
	connect(core()->documentController(), SIGNAL(documentStateChanged(KDevelop::IDocument*)), this, SLOT(changeToEditingMode(KDevelop::IDocument*)));
}

void KDevChecksRunnerPlugin::parsingFinished(KDevelop::ParseJob* job)
{
	KUrl url=job->document().toUrl();
	TopDUContext::Features features = job->minimumFeatures();
	
	if(m_pause || (features & TopDUContext::AllDeclarationsContextsAndUses) != TopDUContext::AllDeclarationsContextsAndUses)
		return;
	
	ReferencedTopDUContext top;
    {
        DUChainReadLocker lock;
        top = DUChain::self()->chainForDocument(url);
    }
	if(!top)
		return;
	
	qDebug() << "seeking tests for" << url << job;
	ControlFlowGraph* flow=job->controlFlowGraph();
	DataAccessRepository* access=job->dataAccessInformation();
    
	if(m_displayMode==Editing)
		m_logModel->clear();
	
	if(access && flow) {
		CodeCheckJob* checkjob=new CodeCheckJob(top, m_logModel, this);
		checkjob->setOnlyCheck(m_onlyCheck);
		checkjob->setAdditionalData(flow, access);
		connect(checkjob, SIGNAL(statistics(QVariantMap)), SLOT(addStatistics(QVariantMap)));
		ICore::self()->runController()->registerJob(checkjob);
	} else {
		delete access;
		delete flow;
	}
}

void KDevChecksRunnerPlugin::clearOutput()
{
	m_logModel->clear();
}

void KDevChecksRunnerPlugin::reloadAll()
{
	setDisplayMode(GlobalReport);
	foreach(KDevelop::IProject* p, ICore::self()->projectController()->projects()) {
		reloadProject(p);
	}
}

void KDevChecksRunnerPlugin::changeToEditingMode(KDevelop::IDocument* doc)
{
	if(doc->state()==IDocument::Modified)
		setDisplayMode(Editing);
}

bool cppBeforeHeader(const IndexedString& a, const IndexedString& b)
{
	bool aIsHeader=a.byteArray().endsWith(".h");
	bool bIsHeader=b.byteArray().endsWith(".h");
	bool ret = aIsHeader==bIsHeader ? a<b : bIsHeader;
	
	return ret;
}

void KDevChecksRunnerPlugin::reloadProject(IProject* p)
{
	KDevelop::BackgroundParser* bg=KDevelop::ICore::self()->languageController()->backgroundParser();
	
	TopDUContext::Features processingLevel = KDevelop::TopDUContext::AllDeclarationsContextsAndUses;
	QList< IndexedString > files = p->fileSet().toList();
	qSort(files.begin(), files.end(), cppBeforeHeader);
	
	foreach(const KDevelop::IndexedString& url, files) {
		int prio = url.str().endsWith(".h") ? 10 : -10;
		bg->addDocument( url, processingLevel, prio );
	}
}

void KDevChecksRunnerPlugin::gotoItem(const QModelIndex& idx)
{
	if(idx.model()->rowCount(idx)!=0)
		return;
	
	KTextEditor::Range range = idx.data(Qt::UserRole+1).value<KTextEditor::Range>();
	KUrl url = idx.parent().parent().data(Qt::UserRole+1).toUrl();
	
	qDebug() << "problem selected" << url << range;
	ICore::self()->documentController()->openDocument(url, range);
}

template <class T>
void addValues(QVariant& v, const QVariant& v2)
{
	v.setValue<T>(v.value<T>()+v2.value<T>());
}

void addStatsValue(const QVariantMap::iterator& it, const QVariant& value)
{
	if(it.value().type()!=value.type())
		*it = value;
	else switch(value.type()) {
		case QVariant::Int:
			addValues<int>(*it, value);
			break;
		case QVariant::UInt:
			addValues<uint>(*it, value);
			break;
		case QVariant::List:
			addValues<QVariantList>(*it, value);
			break;
		default:
			break;
	}
}

void KDevChecksRunnerPlugin::addStatistics(const QVariantMap& stats)
{
	for(QVariantMap::const_iterator it = stats.constBegin(), itEnd=stats.constEnd(); it!=itEnd; ++it) {
		QVariantMap::iterator resIt = m_stats.find(it.key());
		
		if(resIt==m_stats.end()) {
			m_stats.insert(it.key(), it.value());
		} else {
			addStatsValue(resIt, it.value());
		}
	}
}

void KDevChecksRunnerPlugin::exportReport()
{
	QString file=KFileDialog::getSaveFileName();
	if(!file.isEmpty()) {
		exportReport(file);
	}
}

QString escapeQuotes(const QString& str)
{
	QString s(str);
	s.replace('"', "\\\"");
	s.replace('\t', ' ');
	return s;
}

bool KDevChecksRunnerPlugin::exportReport(const QString& file)
{
	QSet< IndexedString > files;
	foreach(IProject* p, ICore::self()->projectController()->projects())
		files += p->fileSet();
	QMap<KUrl, QList<ProblemPointer> > problems;
	
	foreach(const KDevelop::IndexedString& file, files) {
		KUrl url=file.toUrl();
		QList<ILanguage*> langs = KDevelop::ICore::self()->languageController()->languagesForUrl(url);
		
		if(langs.size()>0) {
			DUChainReadLocker lock;
			TopDUContext* ctx=langs.first()->languageSupport()->standardContext(url, true);
			if(ctx)
				problems[url] = ctx->problems();
		}
	}
	
	QFile f(file);
	bool b = f.open(QFile::Text | QFile::WriteOnly);
	if(!b)
		return false;
	
	QTextStream str(&f);
	str << "{\n\t\"files\": [\n";
	
	QMap<KUrl, QList<ProblemPointer> >::const_iterator it=problems.constBegin(), itEnd=problems.constEnd();
	for(; it!=itEnd; ++it) {
		str << "\t{\n";
		str << "\t\t\"name\": \"" << escapeQuotes(it.key().prettyUrl()) << "\",\n";
		str << "\t\t\"problems\": [";
		
		QStringList problemsStr;
		QList<ProblemPointer> problems = it.value();
		foreach(ProblemPointer ptr, problems) {
			problemsStr += "{ \"description\": \"" + escapeQuotes(ptr->description()) + "\", \"line\": " + QString::number(ptr->finalLocation().start.line) + " }";
		}
		
		if(!problemsStr.isEmpty())
			str << "\n\t\t\t" << problemsStr.join(",\n\t\t\t") << "\n\t\t]\n";
		else
			str << "]\n";
		str << "\t}";
		if(it+1!=itEnd)
			str << ',';
		
		str << '\n';
	}
	str << "\t],\n";
	str << "\t\"stats\": {\n";
	
	for(QVariantMap::const_iterator it=m_stats.constBegin(), itEnd=m_stats.constEnd(); it!=itEnd; ++it) {
		str << "\t\t\"" << it.key() << "\": \"" << escapeQuotes(it.value().toString()) << '\"';
		if(it+1!=itEnd)
			str << ',';
		
		str << '\n';
	}
	
	str << "\t}\n}\n";
	
	return true;
}

QList<IAssistantAction::Ptr> fetchAssistants(const QModelIndex& parent, QAbstractItemModel* model)
{
    QList<IAssistantAction::Ptr> ret;
    int rows = model->rowCount(parent);
    for(int i=0; i<rows; ++i) {
        QModelIndex idx = model->index(i, 0, parent);
        if(model->hasChildren(idx))
            ret += fetchAssistants(idx, model);
        else {
            IAssistantAction::Ptr action = idx.data(Qt::UserRole+1).value<IAssistantAction::Ptr>();
            if(action)
                ret += action;
        }
    }
    return ret;
}

void KDevChecksRunnerPlugin::executeAction()
{
    QList<IAssistantAction::Ptr> assistants = fetchAssistants(QModelIndex(), m_logModel);
    foreach(IAssistantAction::Ptr assistant, assistants) {
        assistant->execute();
    }
}
