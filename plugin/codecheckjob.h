/* This file is part of KDevelop
    Copyright 2010 Aleix Pol Gonzalez <aleixpol@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef TESTSJOB_H
#define TESTSJOB_H

#include <KJob>
#include <KUrl>
#include <QSharedPointer>
#include <interfaces/ilanguagecheck.h>
#include <QThread>
#include <language/checks/dataaccessrepository.h>
#include <language/checks/controlflowgraph.h>

class QStandardItemModel;
class QStandardItem;
namespace KDevelop { class ReferencedTopDUContext; }

class CheckJobThread : public QThread
{
	Q_OBJECT
	public:
		explicit CheckJobThread(KDevelop::CheckData* data, QStandardItem* item, const QList<KDevelop::ILanguageCheck*>& checks, QObject* parent = 0);
		
		virtual void run();
		
		QStandardItem* item() const { return testItem; }
		QVariantMap statistics() const { return m_stats; }
	
	private:
		KDevelop::CheckData* m_data;
		QStandardItem* testItem;
		QList<KDevelop::ILanguageCheck*> m_checks;
		QVariantMap m_stats;
};

class CodeCheckJob : public KJob
{
	Q_OBJECT
	public:
		explicit CodeCheckJob(const KDevelop::ReferencedTopDUContext& top, QStandardItemModel* logModel, QObject* parent = 0);
		virtual ~CodeCheckJob();
		virtual void start();
		
		/** If this is called, the only check run will be the one named @p name.
		 * 	If @p name is empty, all checks will be run.
		 */
		void setOnlyCheck(const QString& name) { m_onlyCheck=name; }
		
	public slots:
		void runChecks();
		void checksRunned();
		void setAdditionalData(KDevelop::ControlFlowGraph* controlFlowGraph, KDevelop::DataAccessRepository* dataAccessInformation);
		
	signals:
		void statistics(const QVariantMap& stats);
		
	private:
		QString m_onlyCheck;
		KDevelop::CheckData m_data;
		QStandardItemModel* m_logModel;
};

Q_DECLARE_METATYPE(KTextEditor::Range);

#endif // TESTSJOB_H

