/***************************************************************************
*   Copyright 2008 Aleix Pol <aleixpol@gmail.com>                         *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU Library General Public License as       *
*   published by the Free Software Foundation; either version 2 of the    *
*   License, or (at your option) any later version.                       *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU Library General Public     *
*   License along with this program; if not, write to the                 *
*   Free Software Foundation, Inc.,                                       *
*   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
***************************************************************************/

#include <shell/core.h>
#include <shell/shellextension.h>
#include <language/backgroundparser/parsejob.h>
#include <interfaces/ilanguage.h>
#include <interfaces/iplugincontroller.h>
#include <interfaces/ilanguagecontroller.h>

#include <KApplication>
#include <KCmdLineArgs>
#include <KAboutData>
#include <KDebug>

#include <QStringList>
#include <QFile>
#include <interfaces/iprojectcontroller.h>
#include <shell/sessioncontroller.h>
#include <interfaces/isession.h>
#include <language/backgroundparser/backgroundparser.h>
#include <interfaces/iruncontroller.h>
#include <interfaces/iruncontroller.h>
#include "statechecker.h"

class ConsoleIDEExtension : public KDevelop::ShellExtension
{
	public:
		static void init() { s_instance = new ConsoleIDEExtension(); } 

		virtual QString xmlFile() { return QString(); }
		virtual QString binaryPath() { return QApplication::applicationDirPath() + "/kdevchecker"; };
		virtual QString defaultProfile() { return QString(); }
		virtual KDevelop::AreaParams defaultArea(){
			KDevelop::AreaParams params = {"code", i18n("Code")};
			return params;
		}
		virtual QString projectFileExtension() { return QString("*.kdev4"); }
		virtual QString projectFileDescription() { return QString(); }
		virtual QStringList defaultPlugins()
		{
			return QStringList()
				<< "kdevchecksrunner"
				<< "KDevProjectManagerView"
				<< "kdevcmakeprojectmanager"
				<< "kdevanalitzachecks";
		}

protected:
		ConsoleIDEExtension() {}
};

bool verbose=false;
void messageOutput(QtMsgType type, const char *msg)
{
	switch (type) {
		case QtDebugMsg:
			if(verbose)
				fprintf(stderr, "%s\n", msg);
			break;
		case QtWarningMsg:
			if(verbose)
				fprintf(stderr, "Warning: %s\n", msg);
			break;
		case QtCriticalMsg:
			fprintf(stderr, "Critical: %s\n", msg);
			break;
		case QtFatalMsg:
			fprintf(stderr, "Fatal: %s\n", msg);
			abort();
	}
}

using namespace KDevelop;

int main(int argc, char** argv)
{
	qInstallMsgHandler(messageOutput);
	KAboutData aboutData( "kdevchecker", 0, ki18n( "kdevchecker" ),
						"0.1", ki18n("Run KDevelop code analysis checks"), KAboutData::License_GPL,
						ki18n( "(c) 2011, The KDevelop developers" ), KLocalizedString(), "http://www.kdevelop.org" );
	KCmdLineArgs::init( argc, argv, &aboutData );
	KCmdLineOptions options;
	options.add("project <path>", ki18n("Projects to analyze"), QByteArray());
	options.add("export <file>", ki18n("File where to dump the report"), QByteArray());
	options.add("check <name>", ki18n("Tell the checker just to use this check"), QByteArray());
	options.add("verbose", ki18n("Indicates if debug output is required"), QByteArray());
	KCmdLineArgs::addCmdLineOptions( options );
	
	KApplication app;
	KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
	
	ConsoleIDEExtension::init();
	Core::initialize(0, Core::Default, "checker");
	ICore::self()->activeSession()->setTemporary(true);
	ICore::self()->languageController()->backgroundParser()->setNeededPriority(5);
	
	StateWatcher s;
	QStringList projects=args->getOptionList("project");
	s.exportFile = args->getOption("export");
	verbose=args->isSet("verbose");
	IPlugin* checks=ICore::self()->pluginController()->loadPlugin("kdevchecksrunner");
	Q_ASSERT(checks && "kdevchecksrunner must be present!");
	checks->setProperty("pause", false);
	s.setChecks(checks);
	
	if(args->isSet("check"))
		checks->setProperty("onlyCheck", args->getOption("check"));
	
	foreach(const QString& project, projects) {
		KUrl theproject(QFileInfo(project).absoluteFilePath());
		
		if(!ICore::self()->activeSession()->containedProjects().contains(theproject))
			ICore::self()->projectController()->openProject(theproject);
	}
	
	QObject::connect(ICore::self()->projectController(), SIGNAL(projectOpened(KDevelop::IProject*)), checks,
		SLOT(reloadProject(KDevelop::IProject*)));
	
	return app.exec();
}
