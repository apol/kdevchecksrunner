/* This file is part of KDevelop
    Copyright 2010 Aleix Pol Gonzalez <aleixpol@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "statechecker.h"
#include <interfaces/icore.h>
#include <interfaces/iruncontroller.h>
#include <interfaces/iplugin.h>
#include <interfaces/ilanguagecontroller.h>
#include <language/backgroundparser/backgroundparser.h>
#include <language/backgroundparser/parsejob.h>
#include <plugin/codecheckjob.h>

StateWatcher::StateWatcher(QObject* parent)
	: QObject(parent), checks(0)
{
	KDevelop::BackgroundParser* bg=KDevelop::ICore::self()->languageController()->backgroundParser();
	connect(KDevelop::ICore::self()->runController(), SIGNAL(jobRegistered(KJob*)), SLOT(startedProcess(KJob*)));
	connect(bg, SIGNAL(parseJobFinished(KDevelop::ParseJob*)), SLOT(stateChanged()));
	connect(&m_timeWithoutJobs, SIGNAL(timeout()), bg, SLOT(enableProcessing()));
}

void StateWatcher::setChecks(KDevelop::IPlugin* p)
{
	Q_ASSERT(p);
	checks=p;
}

void StateWatcher::startedProcess(KJob* job)
{
	if(qobject_cast<KDevelop::ParseJob*>(job) || job->metaObject()->className()=="CodeCheckJob") {
		m_timeWithoutJobs.stop();
	}
}

void StateWatcher::stateChanged()
{
	m_timeWithoutJobs.start(20000); //TODO: megahack
	
	KDevelop::BackgroundParser* bg=KDevelop::ICore::self()->languageController()->backgroundParser();
	if(bg->queuedCount()==0) {
		QMetaObject::invokeMethod(checks, "exportReport", Q_ARG(QString, exportFile));
	}
}
