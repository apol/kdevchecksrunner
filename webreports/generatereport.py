import json
import sys

if __name__ == "__main__":
	
	for path in sys.argv[1:]:
		f = open(path, 'r')
		data = json.loads(f.read())
		f.close()
		
		files = data['files']
		
		f = open(path+'.html', 'w')
		
		f.write('<html>\n<body>\n')
		for afile in files:
			problems=afile['problems']
			name = afile['name']
			
			if len(problems)==0:
				f.write("\t<p>[OK] %s</p>\n" % name)
			else:
				f.write("\t<p><span style='color: #f00;'>[NOK]</span> %s</p>\n" % name)
				
				f.write('\t<ul>\n')
				for p in problems:
					f.write("\t\t<li>%d: %s</li>\n" % (p['line'], p['description']))
				
				f.write('\t</ul>\n')
		
		f.write('</body>\n</html>\n')
